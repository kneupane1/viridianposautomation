import { Config } from "../support/config"

export interface ConfigModel extends Cypress.ConfigOptions {
  users: { [user: string]: ConfigUser }
  appUrl: string
  api: {
    url: string
    auth: any
  }
  envName: string
}

export interface ConfigUser {
  username: string
  uid: string
  password: string
}

export const CypressConfig = Config as ConfigModel
