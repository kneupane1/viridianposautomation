/// <reference types="cypress" />

export class VCheckbox {
  constructor(public vCheckboxSelectorOrElement: string | Cypress.Chainable) {}

  public element = (): Cypress.Chainable => {
    return typeof this.vCheckboxSelectorOrElement === "string"
      ? cy.get(this.vCheckboxSelectorOrElement as string)
      : (this.vCheckboxSelectorOrElement as Cypress.Chainable)
  }

  public check(): Cypress.Chainable {
    return this.element().check({ force: true })
  }

  public uncheck(): Cypress.Chainable {
    return this.element().uncheck({ force: true })
  }
}
