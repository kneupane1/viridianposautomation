/// <reference types="cypress" />
export class VMenu {
  constructor(public selector: string) {}
  public exists = () => this.element().should("exist")
  public element(): Cypress.Chainable {
    return cy.get(`${this.selector}`)
  }

  public open(): Cypress.Chainable {
    return this.element().click({ force: true })
  }

  public close(): Cypress.Chainable {
    // The trigger will toggle the open/close
    return this.open()
  }
}
