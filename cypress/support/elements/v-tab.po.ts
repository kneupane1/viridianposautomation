/// <reference types="cypress" />
export class VTab {
  constructor(public selector: string, public tabName: string) {}
  public exists = () => this.element().should("exist")
  public element(): Cypress.Chainable {
    return cy.get(`${this.selector} .v-tab`)
  }

  public click(): Cypress.Chainable {
    return cy
      .get(`${this.selector}`)
      .contains(this.tabName)
      .click({ force: true })
  }
  public isActive(): Cypress.Chainable {
    return this.element().should("have.class", "v-tab--active")
  }
  public isNotActive(): Cypress.Chainable {
    return this.element().should("not.have.class", "v-tab--active")
  }
}
