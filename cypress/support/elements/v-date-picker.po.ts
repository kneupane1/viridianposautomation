/// <reference types="cypress" />
import { Hours, Minutes, Months } from "../date.utils"

export interface DatePickerOptions {
  year?: number
  month?: Months
  day?: number
}

export class VDatePicker {
  public static MOMENT_DATE_FORMAT = "M/D/YY"

  constructor(public datePickerFormFieldSelector: string) {}

  public fillDate(text: string): Cypress.Chainable {
    return cy.get(`${this.datePickerFormFieldSelector} input`).cleanPaste(text)
  }

  /**
   * Sets the vue-date-picker to a specific date, based on the options
   * @param datepickerSelector The selector that will grab the vue-date-picker element
   * @param options options to set the date to a specific time
   */
  public setDate(options: DatePickerOptions): Cypress.Chainable {
    if (options.year) {
      this.selectYear(options.year)
    } else options.month
    this.selectMonth(options.month)
    return this.selectDay(options.day)
  }

  public selectDay(day: number): Cypress.Chainable {
    return cy
      .get(".v-date-picker-table--date")
      .get(".v-btn__content")
      .contains(`${day}`)
      .click({ force: true })
  }

  public selectYear(year: number): Cypress.Chainable {
    return cy
      .get(".v-picker__title__btn")
      .get(".v-date-picker-title__year")
      .click({ force: true })
      .get(".v-picker__body")
      .get(".v-date-picker-years")
      .contains(`${year}`)
      .click({ force: true })
  }

  public selectMonth(month: string): Cypress.Chainable {
    return cy
      .get(".v-date-picker-table--month")
      .get(".v-btn")
      .get(".v-btn__content")
      .contains(month)
      .click({ force: true })
  }
}
