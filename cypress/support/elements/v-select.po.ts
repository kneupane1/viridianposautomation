/// <reference types="cypress" />
export class VSelect {
  constructor(
    public vSelectSelectorOrElement: string | Cypress.Chainable,
    public multiple: boolean
  ) {}

  public static OPEN_CLOSE_WAIT_TIME = 10

  public element(): Cypress.Chainable {
    return typeof this.vSelectSelectorOrElement === "string"
      ? cy.get(this.vSelectSelectorOrElement)
      : (this.vSelectSelectorOrElement as Cypress.Chainable)
  }
  public openPanel(): Cypress.Chainable {
    const vSelectElement = this.element()
    return vSelectElement
      .click({ force: true })
      .wait(VSelect.OPEN_CLOSE_WAIT_TIME)
  }

  public closePanel(): Cypress.Chainable {
    return cy
      .get(".overflow-hidden")
      .click({ force: true })
      .wait(VSelect.OPEN_CLOSE_WAIT_TIME)
  }

  public getvOptions(openPanel = true): Cypress.Chainable<string[]> {
    const options: string[] = []
    if (openPanel) {
      this.openPanel()
    }

    return this.vOptionTexts()
      .each((option) => options.push(option.text()))
      .then(() => this.closePanel())
      .then(() => options)
  }

  /**
   * NOTE: This can only be used on options that have multiple options
   */
  public cleanSelectItems(...optionsToSelect: string[]): void {
    this.openPanel()
    optionsToSelect.forEach((selectOption) => {
      return this.clickOption(selectOption)
    })

    this.closePanel()
  }

  public currentlySelectedValue(): Cypress.Chainable {
    return this.element().find(".v-select__selection")
  }

  public selectItems(...optionsToSelect: string[]): Cypress.Chainable {
    return this.openPanel()
      .then(() => {
        optionsToSelect.forEach((selectOption) => {
          return this.clickOption(selectOption)
        })
      })
      .then(() => {
        if (this.multiple) {
          this.closePanel()
        }
      })
  }
  /**
   * NOTE: This is only used if the v-select is opened
   */
  public vOptionTexts(): Cypress.Chainable {
    return cy.get(".v-list-item")
  }

  /**
   * NOTE: This can only be used if the v-select is opened
   */
  public clickOption(selectOption: string): Cypress.Chainable {
    return this.vOptionTexts()
      .contains(selectOption)
      .first()
      .click({ force: true })
  }

  public selectFirstItem(): Cypress.Chainable {
    return this.openPanel()
      .then(() => {
        return this.vOptionTexts().first().click({ force: true })
      })
      .then(() => {
        if (this.multiple) {
          this.closePanel()
        }
      })
  }

  public selectLastItem(): Cypress.Chainable {
    return this.openPanel()
      .then(() => {
        return this.vOptionTexts().last().click({ force: true })
      })
      .then(() => {
        if (this.multiple) {
          this.closePanel()
        }
      })
  }
}
