/// <reference types="cypress" />
export class VSlideToggle {
  constructor(public vSlideSelectorOrElement: string | Cypress.Chainable) {}

  public element = (): Cypress.Chainable => {
    return typeof this.vSlideSelectorOrElement === "string"
      ? cy.get(this.vSlideSelectorOrElement as string)
      : (this.vSlideSelectorOrElement as Cypress.Chainable)
  }

  public check(): Cypress.Chainable {
    return this.element().check({ force: true })
  }

  public uncheck(): Cypress.Chainable {
    return this.element().uncheck({ force: true })
  }
}
