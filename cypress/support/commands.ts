/// <reference types="cypress"/>
import { CypressConfig } from "../models/config"
import { ApiPaths } from "../models/paths.model"
import { LoginPage } from "./page-objects/login.po"

let token = null

Cypress.Commands.add("appRef", () => {
  return cy.window().should("have.property", "appRef")
})

/**
 * Forces the UI to refresh
 */
Cypress.Commands.add(
  "requestApi",
  (
    method: string,
    relativePath: string,
    options: { headers?; body?; timeout? } = {}
  ): Cypress.Chainable =>
    cy.request({
      method,
      url: `${ApiPaths.basePath}${relativePath}`,
      headers: {
        Authorization: token,
        "Content-Type": "application/json",
        Accept: "application/json",
        ...(options.headers || {})
      },
      body: options.body || undefined
    })
)

Cypress.Commands.add(
  "cleanPaste",
  { prevSubject: "element" },
  (subject, text: string) => {
    return cy
      .wrap(subject, { log: false })
      .invoke("val", text, { force: true })
      .trigger("input", { force: true })
  }
)

// Commands to login through the UI

Cypress.Commands.add(
  "uiLogin",
  (email: string, password: string, visit = true) => {
    if (visit) {
      cy.visit("/Account/Login")
    }

    LoginPage.userNameInput().cleanPaste(email || "")
    LoginPage.passwordInput().cleanPaste(password || "")
    return LoginPage.loginSubmit()
  }
)

// Valid login
Cypress.Commands.add("uiLoginAs", (mainUser, redirectPath: string) => {
  const user = CypressConfig.users[mainUser]
  cy.log(`Logging in with ${user.username} as the '${mainUser}' role`)
  return cy.uiLogin(user.username, user.password)
})

Cypress.Commands.add("tokenLogin", () => {
  const mainUser = CypressConfig.users.mainUser
  cy.log(`Logging in as: ${mainUser.username} (${JSON.stringify(mainUser)})`)
  let tokenResult
  return cy
    .request({
      method: "POST",
      url: `${CypressConfig.api.url}/api/V4/Login`,
      body: {
        UserName: "nbsqa05",
        Password: "nbsqa05"
      }
    })
    .then((apiAuthResponse: any) => {
      cy.log("Received Authentication")
      tokenResult = apiAuthResponse.body
      token = `Bearer ${tokenResult.access_token}`
    })
    .log("Received Authentication")
    .wrap(tokenResult)
})

Cypress.Commands.add("getApiToken", () => {
  return token
})

Cypress.Commands.add("tokenLogout", () => {
  return cy.request({
    method: "POST",
    url: `${CypressConfig.api.url}/Account/LogOff`
  })
})
