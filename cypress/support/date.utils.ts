/// <reference types="cypress" />
import * as moment from "moment"

export type Months =
  | "Jan"
  | "Feb"
  | "Mar"
  | "Apr"
  | "May"
  | "Jun"
  | "Jul"
  | "Aug"
  | "Sep"
  | "Oct"
  | "Nov"
  | "Dec"
export type Hours = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12
export type Minutes = 0 | 15 | 30 | 45
export type AmOrPm = "AM" | "PM"

export class DateUtils {
  public static toDate(date: any): Date {
    if (!date) {
      return null
    }

    // Any other date
    const momentDate = moment(date)
    if (!momentDate.isValid()) {
      return null
    }

    return momentDate.toDate()
  }

  public static format(
    date: moment.Moment,
    type: "short" | null = null
  ): string {
    return date.format(`M/D/YY${type === "short" ? "" : ", h:mm A"}`)
  }

  public static formatAnyDate(ts: any, type: "short" | null = null): string {
    return this.format(moment(this.toDate(ts)), type)
  }

  public static getFutureDate(amountOfDays: number): Date {
    const date = new Date()
    date.setDate(date.getDate() + amountOfDays) // add X days to date
    return date
  }

  public static ordinalSuffixOf(date: moment.Moment): string {
    const day = date.date()
    const j = day % 10
    const k = day % 100
    if (j === 1 && k !== 11) {
      return `${day}st`
    }
    if (j === 2 && k !== 12) {
      return `${day}nd`
    }
    if (j === 3 && k !== 13) {
      return `${day}rd`
    }
    return `${day}th`
  }

  public static getMonth(month: number): Months {
    return [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ][month] as Months
  }
}
