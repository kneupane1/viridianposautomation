/// <reference types="cypress" />

declare namespace Cypress {
  interface CypressResponse<T> extends Response {
    body: T
  }

  interface Chainable<Subject = any> {
    appRef(): Cypress.Chainable<any>
    tick(): void
    getApiToken(): Cypress.Chainable<string>
    requestApi<ResponseData>(
      method: string,
      relativePath: string,
      options: { headers?; body?; timeout?: any }
    ): Cypress.Chainable<CypressResponse<ResponseData>>

    /**
     * Immediately pastes the text into the input (much quicker and clears out the old text)
     *
     * NOTE: This can only be used on Material Inputs (inputs that use matInput)
     * @param text The text to replace the input with
     *
     * @example cy.get("input.matInput.my-input").cleanPaste("text to put into the input")
     * @type {Cypress.Command}
     * @name cy.cleanPaste
     */
    cleanPaste(text: string | number): Chainable

    /**
     * Log into Viridian Edge app using custom email and password
     * @example cy.login("email", "pass")
     * @type {Cypress.Command}
     * @name cy.uiLogin
     */
    uiLogin(
      email: string,
      password: string,
      visit?: boolean
    ): Chainable<Subject>

    /**
     * Log into Viridian Edge using a user with a specific role. We have specific users set apart for this.
     * @param mainUser valid user to login with
     * @example cy.uiLoginAs("mainUser")
     * @type {Cypress.Command}
     * @name cy.uiLoginAs
     */
    uiLoginAs(mainUser: any, visit?: boolean): Chainable<Subject>

    /**
     * Log into Viridian Edge using a token from the API without any other configuration needed.
     * @type {Cypress.Command}
     * @name cy.tokenLoginAs
     * @example cy.tokenLogin()
     */
    tokenLogin(): Chainable<string>

    /**
     * Clear out all local storage and firebase info to log the user out.
     *
     * WARNING: Only use this if you used `tokenLogin` to `login`.
     *
     * @type {Cypress.Command}
     * @name cy.tokenLogout
     * @example cy.tokenLogout()
     */
    tokenLogout(): Chainable<Subject>
  }
}
