export class EmployeeTimeClockPage {
  public static visit = () => cy.visit("/time-clock")
  public static clockInTtile = () =>
    cy.get(".v-card__title").contains("Time Clock")
  public static currentDateHeadline = () =>
    cy.get("[data-cy='current-date']").should("exist")
  public static clockStatusHeadline = () =>
    cy.get("[data-cy='headline']").should("exist")
  public static punchInButton = () =>
    cy.get("[data-cy='punch-In']").should("exist")
  public static clockInButton = () =>
    cy.get("[data-cy='clock-In']").should("exist")
  public static clockOutButton = () =>
    cy.get("[data-cy='clock-Out']").should("exist")
  public static closeButton = () => cy.get("[data-cy='cancel']").should("exist")
  public static crossButton = () => cy.get("[data-cy='cross']").should("exist")
  public static clockInText = () =>
    cy.get("td.text-start").first().contains("Clock In")
  public static clockOutText = () =>
    cy.get("td.text-start").first().contains("Clock Out")
  public static clockTimeLog = () => cy.get(".v-data-table")
}
