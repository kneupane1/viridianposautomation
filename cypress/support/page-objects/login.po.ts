import { VSlideToggle } from "../elements/v-slide-toggle.po"
export class LoginPage {
  public static visit = () => cy.visit("/Account/login")
  public static homePageLoaded = () =>
    cy.location("pathname", { timeout: 150000 }).should("contain", "/Home")

  public static userNameInput = () => cy.get("[data-cy='input-user-name']")
  public static passwordInput = () => cy.get("[data-cy='input-password']")
  public static loginSubmit = () =>
    cy.get("[data-cy='login-button']").click({ timeout: 50000 })

  public static forgotPasswordLink = () =>
    cy.get("[data-cy='forgot-password-link']")

  public static forgotEmailInput = () =>
    cy.get("[data-cy='input-forgot-email']")

  public static rememberMeToggle = () =>
    new VSlideToggle("[data-cy='v-switch']")

  public static forgotPasswordSubmit = () =>
    cy.get("[data-cy='btn-forgot-password']").click({ timeout: 5000 })

  public static userDropdown = () =>
    cy.get("[data-cy='account-name-panel-header']")

  public static logOutLink = () => cy.get("[data-cy='account-logout']")
  public static dialogTitle = () => cy.get("[data-cy='dialog-title']")
  public static confirmLogOut = () =>
    cy.get("[data-cy='btn-confirm-dialog']").click({ force: true })
}
