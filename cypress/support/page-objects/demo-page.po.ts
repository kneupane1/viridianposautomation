/// <reference types="cypress" />
import { VCheckbox } from "../elements/v-checkbox.po"
import { VMenu } from "../elements/v-menu.po"
import { VSelect } from "../elements/v-select.po"
import { VDatePicker } from "../elements/v-date-picker.po"
import { VTab } from "../elements/v-tab.po"
import { VSlideToggle } from "../elements/v-slide-toggle.po"

export class DemoPage {
  public static visit = () => cy.visit("/")
  public static checkBox = () => new VCheckbox("[data-cy='check-box']")
  public static switch = () => new VSlideToggle("[data-cy='v-switch']")
  public static menu = () => new VMenu("[data-cy='v-menu']")
  public static select = () => new VSelect("[data-cy='v-select']", true)
  public static datePciker = () => new VDatePicker("[data-cy='date-picker']")
  public static okButton = () => cy.get(".md-button-content").contains("Ok")
  public static tabButton = () => new VTab("[data-cy='v-tab']", "Item Two")
}
