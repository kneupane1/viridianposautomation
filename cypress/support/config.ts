/// <reference types="cypress"/>
export const defaultPassword = "nbsqa05"

export const environment = {
  production: false,
  api: {
    url: "https://nbs-qa.azurewebsites.net"
  }
}

export const Config: any = {
  ...Cypress.config(),
  users: {
    mainUser: {
      uid: "",
      username: "nbsqa05",
      password: defaultPassword
    }
  },
  ...environment
}
