/// <reference types="cypress"/>
/// <reference types="../../support/index"/>

import Axios from "axios"
import { generateOrders } from "./orderApiPerformance.utils"
describe("Order API Performace", () => {
  it("should handle 2000+ requests per minute", () => {
    cy.tokenLogin()
      .then(() => cy.getApiToken())
      .then({ timeout: 500 * 8 * 1000 }, (token: string) => {
        cy.wrap(generateOrders(10)).then(
          { timeout: 500 * 6 * 1000 },
          async (orders) => {
            const responses = await Promise.all(
              orders.map(async (order) => {
                try {
                  return Axios.put(
                    "http://localhost:64609/api/V4/addorder",
                    order,
                    {
                      headers: {
                        Authorization: token,
                        "Content-Type": "application/json",
                        Accept: "application/json"
                      }
                    }
                  )
                } catch (err) {
                  console.error(err)
                  return { err, message: "ERROR OCCURRED", order }
                }
              })
            )

            console.log(responses)
          }
        )
      })
  })
})
