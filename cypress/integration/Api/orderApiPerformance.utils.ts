export function generateOrders(amount: number): any[] {
  const orders = new Array(amount).fill({}).map((_, i) => {
    return {
      customerid: 12434,
      cartdiscountcode: "",
      note: `${i}`,
      deliverytype: i % 2 === 0 ? "In-store" : "pick-up",
      DateCreated: new Date().toISOString(),
      CaregiverID: "",
      LocationID: 23,
      LineItems: [
        {
          ProductID: 22584,
          IsCannabis: true,
          Batches: "PG-CO2-1G-H-1-P-4",
          Quantity: 1,
          ItemDiscountCode: "",
          Notes: "",
          Price: 10
        }
      ]
    }
  })

  console.log(orders)
  return orders
}
