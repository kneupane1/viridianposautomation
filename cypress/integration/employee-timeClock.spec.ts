/// <reference types="cypress"/>
/// <reference types="../support/index"/>
import { EmployeeTimeClockPage } from "../support/page-objects/employee-timeClock.po"
describe("Home Page Time Clock Display", () => {
  beforeEach(() => {
    cy.uiLoginAs("mainUser", false)
    cy.wait(10000)
  })

  const currentDate = Cypress.moment().format("MMM DD, YYYY")
  const currentTime = Cypress.moment().format("h:mm A")
  const currentDateTime = Cypress.moment().format("M/DD/YYYY h:mm A")
  //Skip 3rd and 4th test if already clocked-out
  //Skip 5th test if already clocked-In and run 3rd and 4th
  it("should have 'punch' button if clicked time clock modal immediately displays", () => {
    EmployeeTimeClockPage.punchInButton()
    EmployeeTimeClockPage.punchInButton().click()
    EmployeeTimeClockPage.clockInTtile()
  })
  it("should have 3 buttons available ClockIn, ClockOut and Close ", () => {
    EmployeeTimeClockPage.punchInButton().click()
    EmployeeTimeClockPage.clockInButton()
    EmployeeTimeClockPage.clockOutButton()
    EmployeeTimeClockPage.closeButton()
  })
  it.skip("should have Clock-In faded color if clocked in ", () => {
    if (
      EmployeeTimeClockPage.clockStatusHeadline().contains(
        "You haven't clocked-in yet"
      )
    ) {
      EmployeeTimeClockPage.punchInButton().click()
      EmployeeTimeClockPage.clockOutButton().should("be.disabled")
      EmployeeTimeClockPage.clockInButton().click()
    }
  })
  it("should have Clock-In faded color if clocked in ", () => {
    if (
      EmployeeTimeClockPage.clockStatusHeadline().contains(
        `"Clock In : ${currentTime}"`
      )
    ) {
      EmployeeTimeClockPage.punchInButton().click()
      EmployeeTimeClockPage.clockInButton().should("be.disabled")
      EmployeeTimeClockPage.clockOutButton().click()
    }
    EmployeeTimeClockPage.clockStatusHeadline().contains(currentTime)
  })
  it("should have Clock-Out faded color if clocked Out ", () => {
    if (
      EmployeeTimeClockPage.clockStatusHeadline().contains(
        `"Clock Out : ${currentTime}"`
      )
    ) {
      EmployeeTimeClockPage.punchInButton().click()
      EmployeeTimeClockPage.clockOutButton().should("be.disabled")
      EmployeeTimeClockPage.clockInButton().click()
    }
    EmployeeTimeClockPage.clockStatusHeadline().contains(currentTime)
  })
  it("should display recent clock in/clock out activity ", () => {
    EmployeeTimeClockPage.punchInButton().click()
    EmployeeTimeClockPage.clockTimeLog().contains(currentDateTime)
  })
})
