/// <reference types="cypress"/>
/// <reference types="../support/index"/>

import { DemoPage } from "../support/page-objects/demo-page.po"
import {DateUtils} from "../support/date.utils"

describe("V Elements", () => {
  beforeEach(() => {
    cy.visit("/playground")
  })

  it("should have checkbox working", () => {
    DemoPage.checkBox().check()
  })
  it("should have switch working", () => {
    DemoPage.switch().check()
  })
  it("should have menu working", () => {
    DemoPage.menu().open()
    DemoPage.menu().close()
  })
  it("should have select working", () => {
    DemoPage.select().openPanel()
    DemoPage.select().selectItems("Bar")
  })
  it("should have date working", () => {
    let date = { year: 1992, month: DateUtils.getMonth(1), day: 17 }
    DemoPage.datePciker().setDate(date)
  })
  it("should have tabs working", () => {
    DemoPage.tabButton().click()
    DemoPage.tabButton().isActive()
  })
})
