/// <reference types="cypress"/>
/// <reference types="../support/index"/>
import { LoginPage } from "../support/page-objects/login.po"

const validCredential = "nbsqa05"

describe("Login Page", () => {
  beforeEach(() => {
    LoginPage.visit()
  })

  it("should login with valid credentials", () => {
    cy.uiLoginAs("mainUser", false)
    LoginPage.homePageLoaded()
  })

  it("should not allow bad values", () => {
    cy.uiLogin("bad@gmail.com", "pass", false)
    cy.location("pathname", { timeout: 150000 }).should("not.contain", "/Home")
  })

  it("should allow to reset password", () => {
    LoginPage.forgotPasswordLink().click()
    LoginPage.dialogTitle().should("have.text", "Forgot your password?")
    LoginPage.forgotEmailInput().cleanPaste("email@sevadev.com")
    LoginPage.forgotPasswordSubmit()
  })

  it.skip("should remember username and password", () => {
    LoginPage.userNameInput().cleanPaste(validCredential)
    LoginPage.passwordInput().cleanPaste(validCredential)
    LoginPage.rememberMeToggle().check()
    LoginPage.loginSubmit()

    cy.window().then((window) => {
      cy.log(window.localStorage.getItem("rememberMeUser"))
      const user = JSON.parse(window.localStorage.getItem("rememberMeUser"))
      expect(user.username).to.eq(validCredential)
      expect(user.password).to.eq(validCredential)
      expect(window.localStorage.getItem("rememberMeToken")).to.eq("true")
    })
    LoginPage.userDropdown().should("exist").click({ force: true })
    LoginPage.logOutLink().should("exist").click({ force: true })
    LoginPage.confirmLogOut()
    cy.url().should("contain", "login")
    LoginPage.userNameInput().should("have.value", validCredential)
    LoginPage.passwordInput().should("have.value", validCredential)
  })

  it.skip("should not remember username and password when unchecked", () => {
    LoginPage.userNameInput().cleanPaste(validCredential)
    LoginPage.passwordInput().cleanPaste(validCredential)
    LoginPage.rememberMeToggle().uncheck()
    LoginPage.loginSubmit()
    cy.window().then((window) => {
      cy.log(window.localStorage.getItem("rememberMeToken"))
      expect(window.localStorage.getItem("rememberMeToken")).to.eq(null)
    })
  })
})
